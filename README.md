# Ariane

A Gemini Protocol browser for Android based OS

## Releases

* Install via direct download from the project page on Öppenlab.net: [oppenlab.net/pr/ariane](https://oppenlab.net/pr/ariane/)
* Install on [Google Play](https://play.google.com/store/apps/details?id=oppen.gemini.ariane)

# Contributing

## Localisation

To add support for another language copy [strings.xml](https://codeberg.org/oppenlab/Ariane/src/branch/trunk/app/src/main/res/values/strings.xml) and replace the default english with suitable translations. You can do this simply by sending me an amended copy of strings.xml (ariane@fastmail.se) or by cloning the project and submitting a pull request with the full implementation.

More information: [developer.android.com/guide/topics/resources/localization#creating-alternatives](https://developer.android.com/guide/topics/resources/localization#creating-alternatives)

## Issue tracker

[https://codeberg.org/oppenlab/Ariane/issues](codeberg.org/oppenlab/Ariane/issues)

# Licence

[Mozilla Public License 2.0](LICENSE)