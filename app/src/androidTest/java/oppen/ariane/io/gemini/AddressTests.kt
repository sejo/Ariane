package oppen.ariane.io.gemini

import android.net.Uri
import oppen.ariane.Ariane
import com.google.common.truth.Truth.assertThat
import org.junit.Test

class AddressTests {

    @Test
    fun uriApiTests(){
        var uri = Uri.parse(Ariane.DEFAULT_HOME_CAPSULE)

        assertThat(uri.toString()).isEqualTo("gemini://gemini.circumlunar.space/~oppen/index.gmi")
        assertThat(uri.isAbsolute).isTrue()
        assertThat(uri.isHierarchical).isTrue()
        assertThat(uri.isRelative).isFalse()

        val lastSegment = uri.lastPathSegment
        assertThat(lastSegment).isEqualTo("index.gmi")

        uri = Uri.parse(uri.pathSegments.joinToString("/"))
        assertThat(uri.toString()).isEqualTo("~oppen/index.gmi")
        assertThat(uri.isRelative).isTrue()
    }

    @Test
    fun arianeUriTests(){
        val builder = AddressBuilder()
        var uri = builder.request("gemini://gemini.circumlunar.space/~oppen/index.gmi").uri()

        assertThat(uri.toString()).isEqualTo("gemini://gemini.circumlunar.space/~oppen/index.gmi")
        assertThat(uri.path).isEqualTo("/~oppen/index.gmi")

        uri = builder.request("hello/index.gmi").uri()

        assertThat(uri.toString()).isEqualTo("gemini://gemini.circumlunar.space/~oppen/hello/index.gmi")

        uri = builder.request("world/index.gmi").uri()
        assertThat(uri.toString()).isEqualTo("gemini://gemini.circumlunar.space/~oppen/hello/world/index.gmi")

        uri = builder.request("./foo/index.gmi").uri()
        assertThat(uri.toString()).isEqualTo("gemini://gemini.circumlunar.space/~oppen/hello/world/foo/index.gmi")

        uri = builder.request("bar/").uri()
        assertThat(uri.toString()).isEqualTo("gemini://gemini.circumlunar.space/~oppen/hello/world/foo/bar/")

    }


    @Test
    fun userTicketUriTests(){
        val builder = AddressBuilder()

        builder.request("gemini://mycapsule.com")
        val relativeWithGmi = builder.request("art/index.gmi").uri()
        assertThat(relativeWithGmi.toString()).isEqualTo("gemini://mycapsule.com/art/index.gmi")

        builder.request("gemini://mycapsule.com")
        val relativeWithoutGmi = builder.request("art/").uri()
        assertThat(relativeWithoutGmi.toString()).isEqualTo("gemini://mycapsule.com/art/")

    }

    @Test
    fun doubleSlashTest(){
        val builder = AddressBuilder()

        val uri = builder.request("//mycapsule.com").uri()
        assertThat(uri.toString()).isEqualTo("gemini://mycapsule.com")
    }

    @Test
    fun pathTest(){
        val builder = AddressBuilder()
        var uri = builder
            .request("gemini://mycapsule.com")
            .request("hello/")
            .uri()
        assertThat(uri.toString()).isEqualTo("gemini://mycapsule.com/hello/")

        uri = builder.request("/world/").uri()

        assertThat(uri.toString()).isEqualTo("gemini://mycapsule.com/world/")

        uri = builder.request("/").uri()

        assertThat(uri.toString()).isEqualTo("gemini://mycapsule.com/")

    }
}