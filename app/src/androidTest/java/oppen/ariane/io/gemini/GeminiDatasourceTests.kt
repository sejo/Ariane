package oppen.ariane.io.gemini

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import oppen.ariane.Ariane
import oppen.toURI
import com.google.common.truth.Truth.assertThat
import oppen.ariane.io.GemState
import oppen.ariane.io.database.ArianeDatabase
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class GeminiDatasourceTests {

    private lateinit var gemini: Datasource
    private val capsules = listOf(
        "gemini://gemini.circumlunar.space",
        "gemini://rawtext.club",
        "gemini://drewdevault.com",
        "gemini://talon.computer",
        "gemini://tilde.team",
        "gemini://tilde.pink",
        "gemini://gemini.conman.org",
        "gemini://idiomdrottning.org"
    )

    private var capsuleIndex = 0

    @Before
    fun setup(){
        val capsule = capsules.random()
        println("Using $capsule for Gemini tests")
        capsuleIndex = capsules.indexOf(capsule)
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        val db = ArianeDatabase(appContext)
        gemini = Datasource.factory(InstrumentationRegistry.getInstrumentation().targetContext, db.history())
    }

    @Test
    fun arianeHomePageTest(){
        var hasRequested = false
        var hasResponded = false

        gemini.request(Ariane.DEFAULT_HOME_CAPSULE.toURI()){ state ->

            when(state){
                is GemState.Requesting -> {
                    assertThat(state.uri.toString()).isEqualTo(Ariane.DEFAULT_HOME_CAPSULE)
                    hasRequested = true
                }
                is GemState.ResponseGemtext -> {
                    assertThat(state.uri.toString()).isEqualTo(Ariane.DEFAULT_HOME_CAPSULE)
                    hasResponded = true
                }
                else -> {
                    //This will cause a failed test if request fails
                    assertThat(hasRequested).isTrue()
                    assertThat(hasResponded).isTrue()
                }
            }
        }
    }

    @Test
    fun aCapsuleTest(){
        var hasRequested = false
        var hasResponded = false

        gemini.request(capsules[capsuleIndex].toURI()){ state ->

            when(state){
                is GemState.Requesting -> {
                    assertThat(state.uri.toString()).isEqualTo(capsules[capsuleIndex])
                    hasRequested = true
                }
                is GemState.ResponseGemtext -> {
                    assertThat(state.uri.toString()).isEqualTo(capsules[capsuleIndex])
                    hasResponded = true
                }
                else -> {
                    //This will cause a failed test if request fails
                    assertThat(hasRequested).isTrue()
                    assertThat(hasResponded).isTrue()
                }
            }
        }
    }
}