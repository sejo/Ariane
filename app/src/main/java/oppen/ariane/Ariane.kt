package oppen.ariane

/**
 * Copyright © 2020 Öppenlab oppenlab.net
 */

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.PreferenceManager

class Ariane: Application() {

    override fun onCreate() {
        super.onCreate()

        val prefs = PreferenceManager.getDefaultSharedPreferences(this)

        when {
            prefs.getBoolean("theme_Light", false) -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            prefs.getBoolean("theme_Dark", false) -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            prefs.getBoolean("theme_FollowSystem", true) -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
        }

    }

    companion object{
        const val DEFAULT_HOME_CAPSULE = "gemini://oppen.digital/ariane/"
        const val GEMINI_USER_SEARCH_BASE = "gemini://gus.guru/search?"

        const val FEATURE_CLIENT_CERTS = true

        const val PREF_KEY_CLIENT_CERT_URI = "client_cert_uri"
        const val PREF_KEY_CLIENT_CERT_HUMAN_READABLE = "client_cert_uri_human_readable"
        const val PREF_KEY_CLIENT_CERT_ACTIVE = "client_cert_active"
        const val PREF_KEY_CLIENT_CERT_PASSWORD = "client_cert_password"
        const val PREF_KEY_USE_CUSTOM_TAB = "use_custom_tabs"
    }
}