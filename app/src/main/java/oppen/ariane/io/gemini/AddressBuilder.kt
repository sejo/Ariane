package oppen.ariane.io.gemini

/**
 * Copyright © 2020 Öppenlab oppenlab.net
 */

import android.net.Uri
import java.net.URI

class AddressBuilder {

    private var uri: Uri = Uri.EMPTY

    fun set(uri: Uri){
        this.uri = uri
    }

    fun request(link: URI): AddressBuilder {
        return request(link.toString())
    }

    fun request(link: String): AddressBuilder {
        val linkUri = Uri.parse(link)
        when {
            link == "/" -> {
                uri = Uri.parse("gemini://${uri.host}/")
            }
            linkUri.isAbsolute -> {
                uri = linkUri
            }
            linkUri.toString().startsWith("//") -> {
                uri = Uri.parse("gemini:$link")
            }
            linkUri.toString().startsWith("/") -> {
                val pathLink = "${uri.scheme}://${uri.host}$linkUri"
                uri = Uri.parse(pathLink)
            }
            else -> {
                val currentAddress = uri.toString()

                val directoryPath = when {
                    currentAddress.lastIndexOf("/") > 8 -> {
                        currentAddress.substring(0, currentAddress.lastIndexOf("/")+1)
                    }
                    else -> {
                        "${uri.scheme}://${uri.host}/"
                    }
                }

                val cleanedLink = when {
                    link.startsWith("./") -> {
                        link.substring(2)
                    }
                    link.startsWith("/") -> {
                        link.substring(1)
                    }
                    else -> {
                        link
                    }
                }

                uri = Uri.parse("$directoryPath$cleanedLink")
            }
        }

        return this
    }

    fun uri(): Uri{
        return uri
    }
}