package oppen.ariane.io.gemini

/**
 * Copyright © 2020 Öppenlab oppenlab.net
 */

import android.net.Uri
import androidx.collection.LruCache
import java.net.URI

object RuntimeCache {

    private var lastUri: URI? = null

    private const val CACHE_SIZE = 4 * 1024 * 1024
    private val lruCache = LruCache<String, Pair<GeminiResponse.Header, List<String>>>(
        CACHE_SIZE
    )

    fun put(uri: URI, header: GeminiResponse.Header, lines: List<String>){
        lastUri = uri
        lruCache.put(uri.toString(), Pair(header, lines))
    }

    fun get(uri: URI): Pair<GeminiResponse.Header, List<String>>? = lruCache[uri.toString()]
    fun get(uri: Uri): Pair<GeminiResponse.Header, List<String>>? = lruCache[uri.toString()]

    fun clear() = lruCache.evictAll()

    fun remove(address: String) {
        lruCache.remove(address)
    }

    fun getLastUri(): URI?{
        return lastUri
    }
}