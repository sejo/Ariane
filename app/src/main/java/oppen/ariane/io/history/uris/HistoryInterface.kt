package oppen.ariane.io.history.uris

/**
 * Copyright © 2020 Öppenlab oppenlab.net
 */

import android.content.Context

interface HistoryInterface {
    fun add(address: String)
    fun get(): List<String>
    fun clear()

    companion object{
        fun default(context: Context): HistoryInterface {
            return BasicURIHistory(context)
        }
    }
}