package oppen.ariane.ui

/**
 * Copyright © 2020 Öppenlab oppenlab.net
 */

import android.app.DownloadManager
import android.content.ActivityNotFoundException
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.databinding.DataBindingUtil
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import oppen.ariane.Ariane
import oppen.ariane.R
import oppen.ariane.databinding.ActivityGemBinding
import oppen.ariane.io.GemState
import oppen.ariane.io.database.ArianeDatabase
import oppen.ariane.io.database.bookmarks.BookmarksDatasource
import oppen.ariane.io.gemini.Datasource
import oppen.ariane.io.gemini.GeminiResponse
import oppen.ariane.io.gemini.RuntimeCache
import oppen.ariane.ui.audio_player.AudioPlayer
import oppen.ariane.ui.bookmarks.BookmarkDialog
import oppen.ariane.ui.bookmarks.BookmarksDialog
import oppen.ariane.ui.content_image.ImageDialog
import oppen.ariane.ui.content_text.TextDialog
import oppen.ariane.ui.modals_menus.LinkPopup
import oppen.ariane.ui.modals_menus.about.AboutDialog
import oppen.ariane.ui.modals_menus.history.HistoryDialog
import oppen.ariane.ui.modals_menus.input.InputDialog
import oppen.ariane.ui.modals_menus.overflow.OverflowPopup
import oppen.ariane.ui.settings.SettingsActivity
import oppen.hideKeyboard
import oppen.toPx
import oppen.visible
import oppen.visibleRetainingSpace
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.net.URLEncoder

const val CREATE_IMAGE_FILE_REQ = 628
const val CREATE_AUDIO_FILE_REQ = 629
const val CREATE_BINARY_FILE_REQ = 630
const val CREATE_BOOKMARK_EXPORT_FILE_REQ = 631
const val CREATE_BOOKMARK_IMPORT_FILE_REQ = 632

class GemActivity : AppCompatActivity() {

    private var inSearch = false
    private val mediaPlayer = MediaPlayer()

    private lateinit var bookmarkDatasource: BookmarksDatasource
    private var bookmarksDialog: BookmarksDialog? = null

    private val model by viewModels<GemViewModel>()
    private lateinit var binding: ActivityGemBinding

    private val adapter = GemtextAdapter(true) {  adapter, uri, longTap, position: Int, view ->
        if(longTap){
            loadingView(true)
            model.requestInlineImage(uri){ imageUri ->
                imageUri?.let{
                    runOnUiThread {
                        loadingView(false)
                        adapter.loadImage(position, imageUri)
                    }
                }
            }
            /*
            LinkPopup.show(view, uri){ menuId ->
                when (menuId) {
                    R.id.link_menu_load_image -> {
                        loadingView(true)
                        model.requestInlineImage(uri){ imageUri ->
                            imageUri?.let{
                                runOnUiThread {
                                    loadingView(false)
                                    adapter.loadImage(position, imageUri)
                                }
                            }
                        }
                    }
                    R.id.link_menu_copy -> {
                        Intent().apply {
                            action = Intent.ACTION_SEND
                            putExtra(Intent.EXTRA_TEXT, uri.toString())
                            type = "text/plain"
                            startActivity(Intent.createChooser(this, null))
                        }
                    }
                }
            }
            */
        }else{
            //Reset input text hint after user has been searching
            if(inSearch) {
                binding.addressEdit.hint = getString(R.string.main_input_hint)
                inSearch = false
            }

            model.request(uri)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val db = ArianeDatabase(applicationContext)
        bookmarkDatasource = db.bookmarks()

        binding = DataBindingUtil.setContentView(this, R.layout.activity_gem)
        binding.viewmodel = model
        binding.lifecycleOwner = this

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)

        binding.gemtextRecycler.layoutManager = LinearLayoutManager(this)
        binding.gemtextRecycler.adapter = adapter

        model.initialise(
            home = PreferenceManager.getDefaultSharedPreferences(this).getString("home_capsule", Ariane.DEFAULT_HOME_CAPSULE) ?: Ariane.DEFAULT_HOME_CAPSULE,
            gemini = Datasource.factory(this, db.history()),
            db = db,
            onState = this::handleState)

        binding.addressEdit.setOnEditorActionListener { _, actionId, _ ->
            when (actionId) {
                EditorInfo.IME_ACTION_GO -> {
                    val input = binding.addressEdit.text.toString()

                    when {
                        input.startsWith("gemini://") -> {
                            if(input.length > 9) model.request(input)//Don't try and request 'gemini://'
                        }
                        else -> model.request("${Ariane.GEMINI_USER_SEARCH_BASE}${URLEncoder.encode(input, "UTF-8")}")
                    }

                    binding.addressEdit.hideKeyboard()
                    binding.addressEdit.clearFocus()

                    return@setOnEditorActionListener true
                }
                else -> return@setOnEditorActionListener false
            }
        }

        binding.addressEdit.setOnFocusChangeListener { v, hasFocus ->
            binding.addressDelete.visible(hasFocus)

            if(!hasFocus){
                binding.addressEdit.hideKeyboard()
            }
        }

        binding.addressDelete.setOnClickListener {
            val current = binding.addressEdit.text.toString()
            if(current == "gemini://"){
                binding.addressEdit.setText("")
            }else if(current.startsWith("gemini://") && current.length > 9){
                binding.addressEdit.setText("gemini://")
                binding.addressEdit.setSelection(9)
            }
        }

        binding.more.setOnClickListener {
            OverflowPopup.show(binding.more){ menuId ->
                when (menuId) {
                    R.id.overflow_menu_search -> {
                        binding.addressEdit.hint = getString(R.string.main_input_search_hint)
                        binding.addressEdit.text?.clear()
                        binding.addressEdit.requestFocus()
                        inSearch = true
                    }
                    R.id.overflow_menu_bookmark -> {
                        val name = adapter.inferTitle()
                        BookmarkDialog(
                            this,
                            BookmarkDialog.mode_new,
                            bookmarkDatasource,
                            binding.addressEdit.text.toString(),
                            name ?: ""
                        ) { _, _ ->
                        }.show()
                    }
                    R.id.overflow_menu_bookmarks -> {
                        bookmarksDialog = BookmarksDialog(this, bookmarkDatasource) { bookmark ->
                            model.request(bookmark.uri)
                        }
                        bookmarksDialog?.show()
                    }
                    R.id.overflow_menu_share -> {
                        Intent().apply {
                            action = Intent.ACTION_SEND
                            putExtra(Intent.EXTRA_TEXT, binding.addressEdit.text.toString())
                            type = "text/plain"
                            startActivity(Intent.createChooser(this, null))
                        }
                    }
                    R.id.overflow_menu_history -> HistoryDialog.show(this, db.history()) { historyAddress ->
                        model.request(historyAddress)
                    }
                    R.id.overflow_menu_about -> AboutDialog.show(this)
                    R.id.overflow_menu_settings -> {
                        startActivity(Intent(this, SettingsActivity::class.java))
                    }
                }
            }
        }

        binding.home.setOnClickListener {
            val home = PreferenceManager.getDefaultSharedPreferences(this).getString("home_capsule", Ariane.DEFAULT_HOME_CAPSULE)
            model.request(home!!)
        }

        binding.pullToRefresh.setOnRefreshListener {
            RuntimeCache.getLastUri()?.run {
                binding.addressEdit.setText(this.toString())
                focusEnd()
                model.request(this)
            }
        }

        checkIntentExtras(intent)
    }

    override fun onResume() {
        super.onResume()

        if(PreferenceManager.getDefaultSharedPreferences(this).getBoolean(Ariane.PREF_KEY_CLIENT_CERT_ACTIVE, false)){
            binding.addressEdit.setCompoundDrawablesWithIntrinsicBounds(R.drawable.vector_client_cert, 0, 0, 0)
            binding.addressEdit.compoundDrawablePadding = 6.toPx().toInt()
        }else{
            hideClientCertShield()
        }

        val showInlineIcons = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("show_inline_icons", false)
        adapter.inlineIcons(showInlineIcons)

        model.invalidateDatasource()
    }

    private fun hideClientCertShield(){
        binding.addressEdit.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
    }

    private fun handleState(state: GemState) {
        binding.pullToRefresh.isRefreshing = false

        when (state) {
            is GemState.AppQuery -> runOnUiThread { showAlert("App backdoor/query not implemented yet") }
            is GemState.ResponseInput -> runOnUiThread {
                loadingView(false)
                InputDialog.show(this, state) { queryAddress ->
                    model.request(queryAddress)
                }
            }
            is GemState.Requesting -> loadingView(true)
            is GemState.NotGeminiRequest -> externalProtocol(state)
            is GemState.ResponseError -> showAlert("${GeminiResponse.getCodeString(state.header.code)}: ${state.header.meta}")
            is GemState.ClientCertError -> {
                hideClientCertShield()
                showAlert("${GeminiResponse.getCodeString(state.header.code)}: ${state.header.meta}")
            }
            is GemState.ResponseGemtext -> renderGemtext(state)
            is GemState.ResponseText -> renderText(state)
            is GemState.ResponseImage -> renderImage(state)
            is GemState.ResponseAudio -> renderAudio(state)
            is GemState.ResponseBinary -> renderBinary(state)
            is GemState.Blank -> {
                binding.addressEdit.setText("")
                adapter.render(arrayListOf())
            }
            is GemState.ResponseUnknownMime -> {
                runOnUiThread {
                    loadingView(false)
                    AlertDialog.Builder(this, R.style.AppDialogTheme)
                        .setTitle(R.string.unknown_mime_dialog_title)
                        .setMessage("Address: ${state.uri}\nMeta: ${state.header.meta}")
                        .setPositiveButton(getString(R.string.download)) { _, _ ->
                            loadingView(true)
                            model.requestBinaryDownload(state.uri)
                        }
                        .setNegativeButton(getString(R.string.cancel)) { _, _ -> }
                        .show()
                }
            }
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        intent?.let{
            checkIntentExtras(intent)
        }
    }

    /**
     *
     * Checks intent to see if Activity was opened to handle selected text
     *
     */
    private fun checkIntentExtras(intent: Intent) {

        //Via ProcessTextActivity from selected text in another app
        if(intent.hasExtra("process_text")){
            val processText = intent.getStringExtra("process_text")
            binding.addressEdit.setText(processText)
            model.request(processText ?: "")
            return
        }

        //From clicking a gemini:// address
        val uri = intent.data
        if(uri != null){
            binding.addressEdit.setText(uri.toString())
            model.request(uri.toString())
            return
        }
    }

    private fun showAlert(message: String) = runOnUiThread{
        loadingView(false)

        if(message.length > 40){
            AlertDialog.Builder(this)
                .setTitle(getString(R.string.error))
                .setMessage(message)
                .setPositiveButton("OK"){ _, _ ->

                }
                .show()
        }else {
            Snackbar.make(binding.root, message, Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun externalProtocol(state: GemState.NotGeminiRequest) = runOnUiThread {
        loadingView(false)
        val uri = state.uri.toString()

        when {
            (uri.startsWith("http://") || uri.startsWith("https://")) &&
                    PreferenceManager.getDefaultSharedPreferences(this).getBoolean(Ariane.PREF_KEY_USE_CUSTOM_TAB, true) -> {
                val builder = CustomTabsIntent.Builder()
                val intent = builder.build()
                intent.launchUrl(this, Uri.parse(uri))
            }
            else -> {
                val viewIntent = Intent(Intent.ACTION_VIEW)
                viewIntent.data = Uri.parse(state.uri.toString())

                try {
                    startActivity(viewIntent)
                }catch (e: ActivityNotFoundException){
                    showAlert(String.format(getString(R.string.not_app_installed_that_can_open),  state.uri))
                }
            }
        }
    }

    private fun renderGemtext(state: GemState.ResponseGemtext) = runOnUiThread {
        loadingView(false)

        //todo - colours didn't change when switching themes, so disabled for now
        //val addressSpan = SpannableString(state.uri.toString())
        //addressSpan.set(0, 9, ForegroundColorSpan(resources.getColor(R.color.protocol_address)))
        binding.addressEdit.setText(state.uri.toString())

        adapter.render(state.lines)

        //Scroll to top
        binding.gemtextRecycler.post {
            binding.gemtextRecycler.scrollToPosition(0)
        }
    }

    private fun renderText(state: GemState.ResponseText) = runOnUiThread {
        loadingView(false)
        TextDialog.show(this, state)
    }

    var imageState: GemState.ResponseImage? = null
    var audioState: GemState.ResponseAudio? = null
    var binaryState: GemState.ResponseBinary? = null

    private fun renderAudio(state: GemState.ResponseAudio) = runOnUiThread {
        loadingView(false)
        AudioPlayer.play(this, binding, mediaPlayer, state){ state ->
            audioState = state
            val intent = Intent(Intent.ACTION_CREATE_DOCUMENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            intent.type = "audio/mpeg"
            intent.putExtra(Intent.EXTRA_TITLE, File(state.uri.path).name)
            startActivityForResult(intent, CREATE_AUDIO_FILE_REQ)
        }
    }

    private fun renderImage(state: GemState.ResponseImage) = runOnUiThread{
        loadingView(false)
        ImageDialog.show(this, state){ state ->
            imageState = state
            val intent = Intent(Intent.ACTION_CREATE_DOCUMENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            intent.type = "image/*"
            intent.putExtra(Intent.EXTRA_TITLE, File(state.uri.path).name)
            startActivityForResult(intent, CREATE_IMAGE_FILE_REQ)
        }
    }

    private fun renderBinary(state: GemState.ResponseBinary) = runOnUiThread{
        loadingView(false)
        binaryState = state
        val intent = Intent(Intent.ACTION_CREATE_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = state.header.meta
        intent.putExtra(Intent.EXTRA_TITLE, File(state.uri.path).name)
        startActivityForResult(intent, CREATE_BINARY_FILE_REQ)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == RESULT_OK && (requestCode == CREATE_IMAGE_FILE_REQ || requestCode == CREATE_AUDIO_FILE_REQ || requestCode == CREATE_BINARY_FILE_REQ)){
            //todo - tidy this mess up... refactor - none of this should be here
            if(imageState == null && audioState == null && binaryState == null) return
                data?.data?.let{ uri ->
                val cachedFile = when {
                    imageState != null -> File(imageState!!.cacheUri.path ?: "")
                    audioState != null -> File(audioState!!.cacheUri.path ?: "")
                    binaryState != null -> File(binaryState!!.cacheUri.path ?: "")
                    else -> {
                        println("File download error - no state object exists")
                        showAlert(getString(R.string.no_state_object_exists))
                        null
                    }
                }

                cachedFile?.let{
                    contentResolver.openFileDescriptor(uri, "w")?.use { fileDescriptor ->
                        FileOutputStream(fileDescriptor.fileDescriptor).use { destOutput ->
                            val sourceChannel = FileInputStream(cachedFile).channel
                            val destChannel = destOutput.channel
                            sourceChannel.transferTo(0, sourceChannel.size(), destChannel)
                            sourceChannel.close()
                            destChannel.close()

                            cachedFile.deleteOnExit()

                            if(binaryState != null){
                                startActivity(Intent(DownloadManager.ACTION_VIEW_DOWNLOADS))
                            }else{
                                Snackbar.make(binding.root, getString(R.string.file_saved_to_device), Snackbar.LENGTH_SHORT).show()
                            }
                        }
                    }
                }
            }

            imageState = null
            audioState = null
            binaryState = null
        }else if(resultCode == RESULT_OK && requestCode == CREATE_BOOKMARK_EXPORT_FILE_REQ){
            data?.data?.let{ uri ->
                bookmarksDialog?.bookmarksExportFileReady(uri)
            }
        }else if(resultCode == RESULT_OK && requestCode == CREATE_BOOKMARK_IMPORT_FILE_REQ){
            data?.data?.let{ uri ->
                bookmarksDialog?.bookmarksImportFileReady(uri)
            }
        }
    }

    private fun loadingView(visible: Boolean) = runOnUiThread {
        binding.progressBar.visibleRetainingSpace(visible)
        if(visible) binding.appBar.setExpanded(true)
    }

    override fun onBackPressed() {
        if(model.canGoBack()){
            model.goBack{ state ->
                /*
                    Passing the callback here so we can eventually add a mechanism to restore scroll position
                */
                handleState(state)
            }
        }else{
            println("Ariane history is empty - exiting")
            super.onBackPressed()
            cacheDir.deleteRecursively()
        }
    }

    private fun focusEnd(){
        binding.addressEdit.setSelection(binding.addressEdit.text?.length ?: 0)
    }
}