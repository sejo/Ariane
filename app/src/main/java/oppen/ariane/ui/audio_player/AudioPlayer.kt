package oppen.ariane.ui.audio_player

/**
 * Copyright © 2020 Öppenlab oppenlab.net
 */

import android.content.Context
import android.media.MediaMetadataRetriever
import android.media.MediaPlayer
import android.view.MenuInflater
import androidx.appcompat.widget.PopupMenu
import androidx.core.view.MenuCompat
import oppen.ariane.R
import oppen.ariane.databinding.ActivityGemBinding
import oppen.ariane.io.GemState
import oppen.visible

object AudioPlayer {

    fun play(context: Context,
             binding: ActivityGemBinding,
             mediaPlayer: MediaPlayer,
             state: GemState.ResponseAudio,
             onSaveAudio: (state: GemState.ResponseAudio) -> Unit){
        val metadataRetriever = MediaMetadataRetriever()
        metadataRetriever.setDataSource(context, state.cacheUri)

        val artist = metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST)
        val trackName = metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE)

        binding.audioTitle.text = "$trackName - $artist"

        mediaPlayer.reset()
        mediaPlayer.setDataSource(context, state.cacheUri)
        mediaPlayer.prepare()

        binding.audioPlayButton.setImageResource(R.drawable.vector_pause)

        mediaPlayer.setOnCompletionListener { _ ->
            binding.audioPlayButton.setImageResource(R.drawable.vector_play)
        }

        binding.audioPlayButton.setOnClickListener {
            if(mediaPlayer.isPlaying){
                binding.audioPlayButton.setImageResource(R.drawable.vector_play)
                mediaPlayer.pause()
            }else{
                binding.audioPlayButton.setImageResource(R.drawable.vector_pause)
                mediaPlayer.start()
            }
        }

        mediaPlayer.setOnInfoListener { _, what, _ ->
            when(what){
                MediaPlayer.MEDIA_INFO_UNKNOWN -> println("MEDIA_INFO_UNKNOWN")
                MediaPlayer.MEDIA_INFO_VIDEO_TRACK_LAGGING-> println("MEDIA_INFO_VIDEO_TRACK_LAGGING")
                MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START-> println("MEDIA_INFO_VIDEO_RENDERING_START")
                MediaPlayer.MEDIA_INFO_BUFFERING_START-> println("MEDIA_INFO_BUFFERING_START")
                MediaPlayer.MEDIA_INFO_BUFFERING_END-> println("MEDIA_INFO_BUFFERING_END")
                MediaPlayer.MEDIA_INFO_BAD_INTERLEAVING-> println("MEDIA_INFO_BAD_INTERLEAVING")
                MediaPlayer.MEDIA_INFO_NOT_SEEKABLE-> println("MEDIA_INFO_NOT_SEEKABLE")
                MediaPlayer.MEDIA_INFO_METADATA_UPDATE-> println("MEDIA_INFO_METADATA_UPDATE")
                MediaPlayer.MEDIA_INFO_UNSUPPORTED_SUBTITLE-> println("MEDIA_INFO_UNSUPPORTED_SUBTITLE")
                MediaPlayer.MEDIA_INFO_SUBTITLE_TIMED_OUT-> println("MEDIA_INFO_SUBTITLE_TIMED_OUT")
            }

            true
        }

        mediaPlayer.setOnErrorListener{ _, i: Int, i1: Int ->
            println("Error Listener.... $i, $i1")
            true
        }

        binding.audioPlayer.visible(true)
        mediaPlayer.start()

        binding.audioOverflow.setOnClickListener {
            val popup = PopupMenu(context, binding.audioOverflow)
            val inflater: MenuInflater = popup.menuInflater
            inflater.inflate(R.menu.audio_overflow, popup.menu)
            popup.setOnMenuItemClickListener { menuItem ->
                when(menuItem.itemId){
                    R.id.audio_overflow_save_file -> {
                        onSaveAudio(state)
                    }
                    R.id.audio_overflow_dismiss -> {
                        mediaPlayer.stop()
                        mediaPlayer.release()
                        binding.audioPlayer.visible(false)
                    }
                }
                true
            }
            MenuCompat.setGroupDividerEnabled(popup.menu, true)
            popup.show()
        }
    }
}