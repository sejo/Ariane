package oppen.ariane.ui.settings

/**
 * Copyright © 2020 Öppenlab oppenlab.net
 */

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import oppen.ariane.R

class SettingsActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_settings)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.vector_close)

        supportFragmentManager.beginTransaction().replace(R.id.settings_container, SettingsFragment()).commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}
