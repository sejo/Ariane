package oppen.ariane.io

import oppen.ariane.io.gemini.GemtextHelper
import org.junit.Test

import org.junit.Assert.*

class GemtextHelperUnitTest {

    @Test
    fun `GemtextHelper correctly finds code blocks`() {
        val source = arrayListOf(
            "Header\n",
            "Some source code:\n",
            "```\n",
            "Code line A\n",
            "Code line B\n",
            "Code line C\n",
            "```\n",
            "Footer\n"
        )

        val parsed = GemtextHelper.findCodeBlocks(source)
        assertEquals(4, parsed.size)


    }

    @Test
    fun `GemtextHelper correctly finds TWO code blocks`() {
        val source = arrayListOf(
            "Header\n",
            "Some source code:\n",
            "```\n",
            "Code line A\n",
            "Code line B\n",
            "Code line C\n",
            "```\n",
            "Interval\n",
            "Some more source:\n",
            "```",
            "Code line D\n",
            "Code line E\n",
            "Code line F\n",
            "```",
            "FOOTER"
        )

        val parsed = GemtextHelper.findCodeBlocks(source)

        println("===============================")
        println("")
        parsed.forEach{line ->
            println("> $line")
        }
        println("")
        println("-------------------------------")
        assertEquals(7, parsed.size)


    }
}